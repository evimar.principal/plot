# -*- coding: utf-8 -*-
"""
Calcula los coeficientes C
de forma de graficar un polinominio sumando esos coeficientes
al valor anterior
"""
import sys


SPACE = ' '
NOTHING = ''
 
def leer_entero():
    return int(leer_sinsalto())
    
def leer_enteros():
    return list(map(int,leer_lista()))
    
def leer_reales():
    return map(float,leer_lista())
    
def leer_lista():
    return leer_sinsalto().split(" ")
    
def leer_sinsalto():
    return leer_linea().strip()    
    
def leer_linea():
    return sys.stdin.readline()
    
def poly(a, x):
    r=a[-1]
    xi = x
    for ai in a[-2::-1]:        
        r = ai*xi + r
        xi = xi*x
    return r
    
COEFS = [[1], [1,2],[1,3,3],[1,4,6,4],[1,5,10,10,5]]

numeros = leer_enteros()    

n = numeros[0]

a = numeros[1:]

c, p = list(), list()

for i in range(n+1):
    p.append(poly(a, i))
    
c.append(p[0])  #C_0
c.append(p[1]-p[0])  #C_1

for i in range(2,n+1):
    j = i-2
    t1 = 0
    for ci, pi in zip(c[1:],COEFS[j]):
        t1 += ci * pi  
    c.append(p[i]-p[i-1]-t1)
  
print(' '.join(map(str,c)))  